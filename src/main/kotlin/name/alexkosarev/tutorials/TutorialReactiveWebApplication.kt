package name.alexkosarev.tutorials

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.server.router

@SpringBootApplication
class TutorialReactiveWebApplication {

    @Bean
    fun routerFunctions(todoHandler: TodoHandler) = router {
        "/api".nest {
            "/todo".nest {
                GET("/", todoHandler::getTodoList)
                GET("/events", todoHandler::subscribeOnEvents)
                GET("/{todoId}", todoHandler::getTodoById)
                POST("/", todoHandler::createTodo)
                PATCH("/{todoId}", todoHandler::modifyTodo)
                PATCH("/{todoId}/completed", todoHandler::markTodoAsCompleted)
                PATCH("/{todoId}/notCompleted", todoHandler::markTodoAsNotCompleted)
                DELETE("/{todoId}", todoHandler::deleteTodo)
            }
        }
    }
}

fun main(args: Array<String>) {
    runApplication<TutorialReactiveWebApplication>(*args)
}
